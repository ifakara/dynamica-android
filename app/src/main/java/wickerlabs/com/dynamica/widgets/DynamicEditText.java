package wickerlabs.com.dynamica.widgets;

import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Patterns;

import wickerlabs.com.dynamica.interfaces.OnFieldChange;
import wickerlabs.com.dynamica.structures.DynamicWidget;

/**
 * Created by: Yusuph Wickama
 * Date: 5/15/2017.
 * Time: 9:35 PM,
 * Website: https://www.wickerlabs.com
 */

public class DynamicEditText extends TextInputEditText implements DynamicWidget {

    private String id;
    private boolean isRequired;
    private String label;
    private String type;
    private String errorMsg = "";
    private OnFieldChange onFieldChangeListener;

    public DynamicEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setOnFieldChangeListener(OnFieldChange onFieldChangeListener) {
        this.onFieldChangeListener = onFieldChangeListener;
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (this.onFieldChangeListener != null) {
            this.onFieldChangeListener.onFieldChange(this.getID(), this.getText().toString());
        }
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);

        if (focused && this.getType().equalsIgnoreCase("date")) {
            this.callOnClick();
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getQuestion() {
        return this.label;
    }

    @Override
    public void setQuestion(String label) {
        this.label = label;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public boolean isInvalid() {
        if (this.isRequired() && TextUtils.isEmpty(this.getText().toString().trim())) {
            this.errorMsg = "Required field cannot be empty.";
            this.setError(this.getErrorMsg());
            return true;
        }

        if(this.getType().equals("email") && !Patterns.EMAIL_ADDRESS.matcher(this.getText()).matches()){
            this.errorMsg = "Invalid email address provided";
            this.setError(this.getErrorMsg());
            return true;
        }

        return false;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public boolean isRequired() {
        return isRequired;
    }

    @Override
    public void setRequired(boolean required) {
        this.isRequired = required;
    }
}
