package wickerlabs.com.dynamica.widgets;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;

import java.io.File;

import wickerlabs.com.dynamica.DynaForms;
import wickerlabs.com.dynamica.R;
import wickerlabs.com.dynamica.handlers.FormSubmitter;
import wickerlabs.com.dynamica.structures.DynamicWidget;

/**
 * Created by: Yusuph Wickama
 * Date: 7/6/17.
 * Time: 2:10 AM,
 * Website: https://www.wickerlabs.com
 */

public class DynamicImageView extends AppCompatImageView implements DynamicWidget {
    private String id;
    private boolean isRequired = false;
    private boolean isValid = false;
    private String label;
    private String type;
    private String imagePath;

    public DynamicImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public DynamicImageView(Context context) {
        super(context);
    }

    public void initiate(){
        this.setImageDrawable(getResources().getDrawable(R.drawable.default_image));
    }

    public void loadImage(Activity activity, File picFile) {
        FormSubmitter submitter = FormSubmitter.getInstance();

        if (picFile != null) {
            DynaForms.Log("[+] Image File : -> ".concat(picFile.getAbsolutePath()));

            Glide.with(activity).load(picFile).centerCrop().into(this);

            submitter.updateValue(this.getID(), picFile.getAbsolutePath());

            this.isValid = true;
        } else {
            if (submitter.persists(this.getID())) {
                File file = new File(submitter.getValue(this.getID()));

                Glide.with(activity).load(file).centerCrop().into(this);

                submitter.updateValue(this.getID(), file.getAbsolutePath());

                this.isValid = true;

            } else {
                DynaForms.Log("[+] Image persistence not supported");
            }
        }

    }

    public String getImagePath() {
        return FormSubmitter.getInstance().getValue(this.getID());
    }

    @Override
    public String getQuestion() {
        return this.label;
    }

    @Override
    public void setQuestion(String label) {
        this.label = label;
    }

    @Override
    public boolean isInvalid() {
        return !isValid;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public boolean isRequired() {
        return isRequired;
    }

    @Override
    public void setRequired(boolean required) {
        this.isRequired = required;
    }
}
