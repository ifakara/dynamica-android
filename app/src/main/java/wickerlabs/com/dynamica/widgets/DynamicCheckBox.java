package wickerlabs.com.dynamica.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import wickerlabs.com.dynamica.structures.DynamicWidget;

/**
 * Created by: Yusuph Wickama
 * Date: 6/3/2017.
 * Time: 2:36 PM,
 * Website: https://www.wickerlabs.com
 */

public class DynamicCheckBox extends AppCompatCheckBox implements DynamicWidget {

    private boolean isRequired;
    private String label;
    private String id;

    public DynamicCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setText("Dynamic Check");
    }

    @Override
    public String getQuestion() {
        return this.label;
    }

    @Override
    public void setQuestion(String label) {
        this.label = label;
    }

    @Override
    public boolean isInvalid() {
        return (this.isRequired() && !this.isChecked());
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public boolean isRequired() {
        return isRequired;
}

    @Override
    public void setRequired(boolean required) {
        this.isRequired = required;
    }


}
