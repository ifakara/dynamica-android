package wickerlabs.com.dynamica.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;

import java.util.HashMap;

import wickerlabs.com.dynamica.model.OfferedAnswers;
import wickerlabs.com.dynamica.structures.DynamicWidget;

public class DynamicSpinner extends AppCompatSpinner implements DynamicWidget {

    private boolean isRequired, modifiesFlow = false;
    private String label;
    private String id;
    private HashMap<String,OfferedAnswers.SkipPage> skipPageHashMap = new HashMap<>();

    public DynamicSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean modifiesFlow() {
        return modifiesFlow;
    }

    public void addSkipMap(String answer, OfferedAnswers.SkipPage skipPage){
        skipPageHashMap.put(answer,skipPage);
    }

    public int getSkipPageByAnswer(String answer){
        return skipPageHashMap.get(answer).getPageNum();
    }

    public void setModifiesFlow(boolean modifiesFlow) {
        this.modifiesFlow = modifiesFlow;
    }

    @Override
    public String getQuestion() {
        return this.label;
    }

    @Override
    public void setQuestion(String label) {
        this.label = label;
    }

    @Override
    public boolean isInvalid() {
        return this.isRequired() && !this.isSelected();
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public boolean isRequired() {
        return this.isRequired;
    }

    @Override
    public void setRequired(boolean required) {
        this.isRequired = required;
    }

}
