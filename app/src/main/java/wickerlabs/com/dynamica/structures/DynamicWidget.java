package wickerlabs.com.dynamica.structures;

/**
 * Created by: Yusuph Wickama
 * Date: 6/3/2017.
 * Time: 9:35 PM,
 * Website: https://www.wickerlabs.com
 */

public interface DynamicWidget {

    String getQuestion();

    void setQuestion(String label);

    boolean isInvalid();

    String getID();

    void setID(String id);

    boolean isRequired();

    void setRequired(boolean required);

    enum inChildOf{

    }

}
