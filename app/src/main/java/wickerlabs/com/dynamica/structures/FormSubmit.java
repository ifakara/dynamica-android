package wickerlabs.com.dynamica.structures;

import android.view.ViewGroup;

import org.json.JSONException;

/**
 * Created by: Yusuph Wickama
 * Date: 6/8/2017.
 * Time: 2:14 PM,
 * Website: https://www.wickerlabs.com
 */

public interface FormSubmit {

    boolean commitForm(ViewGroup parent, String formName, int formRev, int formId, boolean isFinal) throws JSONException;

    void removePageResponse(ViewGroup layout, String pageId, String formName, int formId) throws JSONException;

}
