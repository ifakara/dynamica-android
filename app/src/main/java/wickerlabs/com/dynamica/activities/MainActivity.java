package wickerlabs.com.dynamica.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.javiersantos.bottomdialogs.BottomDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wickerlabs.com.dynamica.DynaForms;
import wickerlabs.com.dynamica.R;
import wickerlabs.com.dynamica.handlers.FormSubmitter;
import wickerlabs.com.dynamica.layout.DynamicLinearLayout;
import wickerlabs.com.dynamica.model.Page;
import wickerlabs.com.dynamica.utils.FileUtils;
import wickerlabs.com.dynamica.utils.GlobalParser;

public class MainActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;
    private static MainActivity mainActivity;

    @BindView(R.id.layoutContainer)
    DynamicLinearLayout layoutContainer;
    @BindView(R.id.pageTxt)
    TextView pageTxt;
    @BindView(R.id.forwardBtn)
    ImageButton forwardBtn;
    @BindView(R.id.backBtn)
    ImageButton backBtn;

    private JSONObject jsonObject;
    private String formName;
    private int formRev;
    private int formId;
    private String curImgId;
    private LinkedList<Page> pageNavList = new LinkedList<>();

    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mainActivity = this;


        new Thread(new Runnable() {
            @Override
            public void run() {
                Intent intent = getIntent();
                String jsonSchema = intent.getStringExtra("jsonSchema");
                formName = intent.getStringExtra("formName");
                formRev = intent.getIntExtra("formRev", 0);
                formId = intent.getIntExtra("formId", 0);

                try {
                    jsonObject = new JSONObject(jsonSchema);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        while (jsonObject == null) {
            //just wait for me to finish
        }

        try {
            ArrayList<Page> pages = GlobalParser.parsePage(jsonObject);

            for (Page page : pages) {
                DynaForms.addPageMapping(page.getPageNumber(), page);
            }

            DynaForms.addPageCount(formId, pages.size());

            Page firstPage = DynaForms.getPageByNumber(1);
            pageNavList.addLast(firstPage);

            layoutContainer.setPage(MainActivity.this, firstPage);
            updatePageLabel();
            backBtn.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePageLabel() {
        int curPageNum = layoutContainer.getPage().getPageNumber();
        pageTxt.setText("Page ".concat(String.valueOf(curPageNum)));
    }

    @OnClick(R.id.forwardBtn)
    public void buttonOnClick(View view) {
        try {
            int nextPage = layoutContainer.getNextPage(formId);

            if (nextPage == DynamicLinearLayout.NO_NEXT_PAGE) {
                new BottomDialog.Builder(this)
                        .setTitle("Form submission")
                        .setContent("You are about to submit data to " + formName + ", Are you sure?")
                        .onPositive(new BottomDialog.ButtonCallback() {
                            @Override
                            public void onClick(@NonNull BottomDialog bottomDialog) {
                                try {
                                    if (!FormSubmitter.getInstance().commitForm(layoutContainer, formName, formRev, formId, true)) {
                                        Toast.makeText(MainActivity.this, "Ooops!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(MainActivity.this, "Form was submitted", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .autoDismiss(true)
                        .setPositiveText("Yes")
                        .setNegativeText("No")
                        .setPositiveBackgroundColorResource(R.color.colorPrimary)
                        .show();

            } else {
                if (FormSubmitter.getInstance().commitForm(layoutContainer, formName, formRev, formId, false)) {
                    Page nextGoToPage = DynaForms.getPageByNumber(nextPage);

                    pageNavList.addLast(nextGoToPage);

                    layoutContainer.setPage(MainActivity.this, DynaForms.getPageByNumber(nextPage));
                    backBtn.setEnabled(true);
                } else {
                    Toast.makeText(MainActivity.this, "Ooops!", Toast.LENGTH_SHORT).show();
                }
            }

            updatePageLabel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.backBtn)
    public void backBtnClick(View view) {
        if (pageNavList.size() == 1) {
            super.onBackPressed();
        } else {
            try {
                DynaForms.Log("I was supposed to remove that...");
                //JSONArray array = FormSubmitter.getInstance().getArray(formId, formName);
                layoutContainer.setPage(MainActivity.this, pageNavList.get(pageNavList.size() - 2));

                FormSubmitter.getInstance().removePageResponse(layoutContainer, pageNavList.removeLast().getId(), formName, formId);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        updatePageLabel();
    }


    public void newPhoto(String id) {
        curImgId = id;
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && data != null) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            assert photo != null;
            photo.compress(Bitmap.CompressFormat.JPEG, 85, baos);
            byte[] imageArray = baos.toByteArray();

            File file = new File(getFilesDir().getAbsolutePath().concat("/" + FileUtils.toFileSystemSafeName(UUID.randomUUID().toString()).substring(0, 6) + ".jpg"));
            try {
                FileUtils.write(file, imageArray);

                FormSubmitter.getInstance().updateValue(curImgId, file.getAbsolutePath());

                DynamicLinearLayout.notifyUpdate();
            } catch (IOException e) {
                e.printStackTrace();
            }

            DynaForms.Log("[+] Photo Received --> " + file.getAbsolutePath());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        new BottomDialog.Builder(MainActivity.this)
                .setTitle("Save form")
                .setContent("Do you want to save this form before you leave?")
                .onPositive(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull BottomDialog bottomDialog) {
                        DynaForms.Log("[+] Should save the form here");
                        Toast.makeText(MainActivity.this, "Form has been saved", Toast.LENGTH_SHORT).show();
                        try {
                            FormSubmitter.getInstance().removePageResponse(layoutContainer, layoutContainer.getPage().getId(), formName, formId);
                            MainActivity.super.onBackPressed();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .onNegative(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull BottomDialog bottomDialog) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .autoDismiss(true)
                .setPositiveText("Yes")
                .setNegativeText("No")
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .show();
    }
}
