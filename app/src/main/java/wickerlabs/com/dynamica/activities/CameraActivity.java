package wickerlabs.com.dynamica.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.florent37.camerafragment.CameraFragment;
import com.github.florent37.camerafragment.configuration.Configuration;

import wickerlabs.com.dynamica.R;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        startCameraFragment();

    }

    public void startCameraFragment() {
        CameraFragment cameraFragment = CameraFragment.newInstance(new Configuration.Builder().build());

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, cameraFragment)
                .commit();
    }
}
