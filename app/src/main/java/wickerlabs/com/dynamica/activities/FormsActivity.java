package wickerlabs.com.dynamica.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import wickerlabs.com.dynamica.R;
import wickerlabs.com.dynamica.adapters.FormsListAdapter;
import wickerlabs.com.dynamica.backend.Backend;
import wickerlabs.com.dynamica.interfaces.GetFormsCallback;
import wickerlabs.com.dynamica.model.DynamicForm;
import wickerlabs.com.dynamica.utils.DialogUtils;

public class FormsActivity extends AppCompatActivity {

    @BindView(R.id.formsListView)
    ListView formsListView;
    List<DynamicForm> formList;
    private FormsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forms);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadViews();
            }
        });

        ButterKnife.bind(this);

        formList = new ArrayList<>();

        adapter = new FormsListAdapter(this,R.layout.forms_list_item,formList);

        formsListView.setAdapter(adapter);

        formsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DynamicForm form = adapter.getItem(position);

                Intent intent = new Intent(FormsActivity.this,MainActivity.class);
                intent.putExtra("jsonSchema",form.getSchema());
                intent.putExtra("formName",form.getName());
                intent.putExtra("formRev",form.getRevision());
                intent.putExtra("formId",form.getId());

                startActivity(intent);
            }
        });


        loadViews();
    }

    private void loadViews(){
        DialogUtils.getInstance().startProgress(this,"Loading forms...");

        Backend.getInstance().getLatestForm(this, new GetFormsCallback() {
            @Override
            public void onDownloadComplete(final List<DynamicForm> forms, Exception e) {
                if(e == null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            formList.clear();
                            adapter.clear();
                            adapter.addAll(forms);
                            adapter.notifyDataSetChanged();
                        }
                    });

                } else {
                    Snackbar.make(findViewById(R.id.mainFormsLayout), "Failed to connect", Snackbar.LENGTH_LONG).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadViews();
                        }
                    }).setActionTextColor(getResources().getColor(R.color.money)).show();

                    e.printStackTrace();
                }

                DialogUtils.getInstance().stopProgress();
            }
        });
    }

}
