package wickerlabs.com.dynamica.backend;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import wickerlabs.com.dynamica.interfaces.DynamicBackend;
import wickerlabs.com.dynamica.interfaces.GetFormsCallback;
import wickerlabs.com.dynamica.interfaces.RequestCallback;
import wickerlabs.com.dynamica.model.DynamicForm;
import wickerlabs.com.dynamica.utils.Constants;

public class Backend implements DynamicBackend {

    private static final Backend ourInstance = new Backend();
    private OkHttpClient httpClient;

    private Backend() {
        httpClient = new OkHttpClient();
    }

    public static Backend getInstance() {
        return ourInstance;
    }

    @Override
    public boolean isLatest(int revisionCode, final RequestCallback requestCallback) {
        RequestBody body = new FormBody.Builder()
                .add("study", Constants.RABIES_STUDY)
                .build();

        Request getVersion = new Request.Builder()
                .url(Constants.BASE_URL)
                .post(body)
                .build();

        httpClient.newCall(getVersion).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                requestCallback.onRequestComplete(null, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                requestCallback.onRequestComplete(response.message(), null);
            }
        });

        return false;
    }

    @Override
    public List<DynamicForm> getLatestForm(final Activity activity, final GetFormsCallback formsCallback) {
        RequestBody body = new FormBody.Builder()
                .add("client",Constants.DEFAULT_CLIENT)
                .build();

        Request getVersion = new Request.Builder()
                .url(Constants.RETRIEVE_URL)
                .post(body)
                .build();

        httpClient.newCall(getVersion).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                formsCallback.onDownloadComplete(null,e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                try {

                    ArrayList<DynamicForm> forms = new ArrayList<>();
                    String schema = response.body().string();
                    JSONArray formArrays = new JSONArray(schema);

                    Log.d("[+] Found forms",String.valueOf(formArrays.length()));

                    // Add forms from the json Array
                    for(int i=0; i <formArrays.length() ;i++){
                        JSONObject formObject = formArrays.getJSONObject(i);

                        String formName = formObject.getString("formName");
                        String formDesc = formObject.getString("formDesc");
                        String modified = formObject.getString("modified");
                        int  accessGroup = formObject.getInt("accessGroup");
                        int formID = formObject.getInt("formId");
                        String formSchema = formObject.getString("jsonSchema");
                        int revision = formObject.getInt("formRev");

                        DynamicForm form = new DynamicForm(formID,formName,accessGroup);
                        form.setDescription(formDesc);
                        form.setSchema(formSchema);
                        form.setRevision(revision);
                        form.setModified(modified);

                        forms.add(form);
                    }

                    formsCallback.onDownloadComplete(forms,null);

                } catch (JSONException e) {
                    formsCallback.onDownloadComplete(null,e);
                    e.printStackTrace();
                }
            }
        });
        return null;
    }

    @Override
    public void submitForms(List<DynamicForm> dynamicForms) {

    }
}
