package wickerlabs.com.dynamica.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import wickerlabs.com.dynamica.R;
import wickerlabs.com.dynamica.model.DynamicForm;

/**
 * Created by Wickerman on 6/5/2017.
 */

public class FormsListAdapter extends ArrayAdapter<DynamicForm> {

    List<DynamicForm> dynamicForms;
    int layoutRes;
    Context context;

    @BindView(R.id.formNameView)TextView formName;
    @BindView(R.id.formDesc)TextView formDesc;
    @BindView(R.id.modifiedView)TextView formTime;


    public FormsListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<DynamicForm> objects) {
        super(context, resource, objects);

        this.layoutRes = resource;
        this.dynamicForms = objects;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dynamicForms.size();
    }

    @Nullable
    @Override
    public DynamicForm getItem(int position) {
        return dynamicForms.get(position);
    }

    @NonNull
    @Override
    @SuppressLint("ViewHolder")
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutRes,parent,false);

        ButterKnife.bind(this,view);

        DynamicForm form = getItem(position);

        formName.setText(form.getName());
        formDesc.setText(form.getDescription());
        formTime.setText("Rev: "+form.getRevision());

        return view;


    }
}
