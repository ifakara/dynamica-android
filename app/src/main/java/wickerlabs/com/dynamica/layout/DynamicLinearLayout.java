package wickerlabs.com.dynamica.layout;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.github.javiersantos.bottomdialogs.BottomDialog;

import org.json.JSONException;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import wickerlabs.com.dynamica.DynaForms;
import wickerlabs.com.dynamica.R;
import wickerlabs.com.dynamica.activities.MainActivity;
import wickerlabs.com.dynamica.handlers.FormSubmitter;
import wickerlabs.com.dynamica.handlers.WidgetsProvider;
import wickerlabs.com.dynamica.interfaces.ImageListener;
import wickerlabs.com.dynamica.interfaces.OnFieldChange;
import wickerlabs.com.dynamica.model.Element;
import wickerlabs.com.dynamica.model.OfferedAnswers;
import wickerlabs.com.dynamica.model.Page;
import wickerlabs.com.dynamica.model.PageFlow;
import wickerlabs.com.dynamica.widgets.DynamicEditText;
import wickerlabs.com.dynamica.widgets.DynamicImageView;
import wickerlabs.com.dynamica.widgets.DynamicSpinner;

/**
 * Created by: Yusuph Wickama
 * Date: 13-Jun-17.
 * Time: 19:50,
 * Website: https://www.wickerlabs.com
 */

@SuppressWarnings("deprecation")
public class DynamicLinearLayout extends LinearLayout {

    public static final int NO_NEXT_PAGE = 332;
    private static ImageListener listener;
    private Page page;

    public DynamicLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public static void notifyUpdate() {

        if (listener != null) listener.notifyChange();

    }

    public Page getPage() {
        return page;
    }

    public void setPage(Activity activity, Page page) throws JSONException {
        this.page = page;

        ArrayList<Element> elements = page.getElements();
        this.removeAllViews();

        for (Element element : elements) {
            DynaForms.addElementToPage(element.getId(), page.getId());

            View elementView = getViewFromElement(activity, element);
            this.addView(elementView);
        }

    }

    public int getNextPage(int formId) throws JSONException {
        DynamicLinearLayout layout = this;
        int viewCount = layout.getChildCount();

        Page curPage = layout.getPage();
        PageFlow flow = curPage.getPageFlow();
        int pageCount = DynaForms.getPageCount(formId);


        for (int i = 0; i < viewCount; i++) {
            View view = layout.getChildAt(i);

            if (view instanceof DynamicSpinner) {
                DynaForms.Log("The spinner: \"".concat(((DynamicSpinner) view).getQuestion()).concat("\" has changed the flow"));

                // handle spinner
                DynamicSpinner spinner = (DynamicSpinner) view;

                if (spinner.modifiesFlow()) {
                    return spinner.getSkipPageByAnswer(spinner.getSelectedItem().toString());
                }
            }

        }

        if (flow.isNextPage() && curPage.getPageNumber() != pageCount) {
            DynaForms.Log("[+] Going to Page ".concat(String.valueOf(curPage.getPageNumber() + 1)));
            return curPage.getPageNumber() + 1;
        }

        return NO_NEXT_PAGE;

    }

    private View getViewFromElement(final Activity activity, Element element) throws JSONException {

        String type = element.getQuestion().getType();
        String id = element.getId();
        final String question = element.getQuestion().toString();
        boolean required = element.getQuestion().isRequired();

        switch (type) {
            case "text":
            case "email":
            case "number":
            case "url":
            case "date": {

                this.addView(WidgetsProvider.getInstance().getLabel(activity, question));

                final DynamicEditText[] textField = {WidgetsProvider.getInstance().getTextField(activity)};
                textField[0].setID(id);
                textField[0].setHint(type.substring(0, 1).toUpperCase().concat(type.substring(1)));
                textField[0].setQuestion(question);
                textField[0].setRequired(required);
                textField[0].setType(type.toLowerCase());
                textField[0].setOnFieldChangeListener(new OnFieldChange() {
                    @Override
                    public void onFieldChange(String fieldId, String value) {
                        FormSubmitter.getInstance().updateValue(fieldId, value);
                    }
                });

                if (FormSubmitter.getInstance().persists(id))
                    textField[0].setText(FormSubmitter.getInstance().getValue(id));

                switch (type.toLowerCase()) {
                    case "email":
                        textField[0].setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        break;
                    case "number":
                        textField[0].setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case "url":
                        textField[0].setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);
                        break;
                    case "date":
                        textField[0].setInputType(InputType.TYPE_CLASS_DATETIME);
                        textField[0].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                textField[0] = (DynamicEditText) v;
                                new SingleDateAndTimePickerDialog.Builder(activity)
                                        .displayHours(false)
                                        .displayMinutes(false)
                                        .curved()
                                        .bottomSheet()
                                        .title(question)
                                        .titleTextColor(activity.getResources().getColor(R.color.colorPrimaryDark))
                                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                                            @Override
                                            public void onDateSelected(Date date) {
                                                DateFormat dateFormat = DateFormat.getDateInstance();

                                                textField[0].setText(dateFormat.format(date));
                                            }
                                        })
                                        .display();
                            }
                        });
                        break;
                    default:
                        textField[0].setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        break;
                }

                return textField[0];
            }

            case "select":
            case "checkbox": {
                ArrayList<String> items = new ArrayList<>();
                ArrayList<OfferedAnswers> answers = element.getQuestion().getOfferedAnswers();
                DynamicSpinner spinner = WidgetsProvider.getInstance().getSpinner(activity);

                for (OfferedAnswers answer : answers) {
                    items.add(answer.getValue());

                    if (answer.modifiesFlow()) {
                        spinner.setModifiesFlow(true);
                        spinner.addSkipMap(answer.getValue(), answer.getSkipPage());
                    }
                }

                this.addView(WidgetsProvider.getInstance().getLabel(activity, question));

                ArrayAdapter<String> adapter = new ArrayAdapter<>(activity.getBaseContext(), android.R.layout.simple_spinner_dropdown_item);
                adapter.addAll(items);

                spinner.setID(id);
                spinner.setAdapter(adapter);
                spinner.setQuestion(question);
                spinner.setRequired(required);

                return spinner;
            }

            case "photo":
                this.addView(WidgetsProvider.getInstance().getLabel(activity, question));

                final DynamicImageView imageView = WidgetsProvider.getInstance().getImageView(activity);
                imageView.setID(id);
                imageView.setQuestion(question);
                imageView.setRequired(required);

                if (FormSubmitter.getInstance().persists(id)) {
                    File file = new File(FormSubmitter.getInstance().getValue(id));
                    if (file.exists()) {
                        DynaForms.Log("[+] File found -> " + file.getAbsolutePath());
                        imageView.loadImage(activity, file);
                    } else {
                        DynaForms.Log("[+] File not found -> :(");
                    }
                }

                listener = new ImageListener() {
                    @Override
                    public void notifyChange() {
                        imageView.loadImage(activity, null);
                    }
                };

                imageView.initiate();
                imageView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new BottomDialog.Builder(getContext())
                                .setTitle(question)
                                .setContent("Do you want to take a new photo?")
                                .onPositive(new BottomDialog.ButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull BottomDialog bottomDialog) {
                                        MainActivity.getMainActivity().newPhoto(imageView.getID());
                                    }
                                })
                                .autoDismiss(true)
                                .setPositiveText("Yes")
                                .setNegativeText("No")
                                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                                .show();

                    }
                });

                return imageView;

            default:
                return new View(activity);

        }


    }
}
