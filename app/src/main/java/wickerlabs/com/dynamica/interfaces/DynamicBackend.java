package wickerlabs.com.dynamica.interfaces;

import android.app.Activity;

import org.json.JSONException;

import java.util.List;

import wickerlabs.com.dynamica.model.DynamicForm;

/**
 * Created by Wickerman on 5/15/2017.
 */

public interface DynamicBackend {

    boolean isLatest(int revisionCode, RequestCallback callback);

    List<DynamicForm> getLatestForm(Activity activity,GetFormsCallback callback) throws JSONException;

    void submitForms(List<DynamicForm> forms);


}
