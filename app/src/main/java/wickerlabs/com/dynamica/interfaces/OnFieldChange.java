package wickerlabs.com.dynamica.interfaces;

/**
 * Created by: Yusuph Wickama
 * Date: 7/1/17.
 * Time: 10:36 AM,
 * Website: https://www.wickerlabs.com
 */

public interface OnFieldChange {
    void onFieldChange(String fieldId, String value);
}
