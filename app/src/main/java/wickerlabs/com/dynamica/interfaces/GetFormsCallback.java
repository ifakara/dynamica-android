package wickerlabs.com.dynamica.interfaces;

import java.util.List;

import wickerlabs.com.dynamica.model.DynamicForm;

/**
 * Created by Wickerman on 5/15/2017.
 */

public interface GetFormsCallback {

    void onDownloadComplete(List<DynamicForm> forms, Exception e);

}
