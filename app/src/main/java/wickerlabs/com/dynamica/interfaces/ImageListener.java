package wickerlabs.com.dynamica.interfaces;

/**
 * Created by: Yusuph Wickama
 * Date: 7/6/17.
 * Time: 4:19 AM,
 * Website: https://www.wickerlabs.com
 */

public interface ImageListener {
    void notifyChange();
}
