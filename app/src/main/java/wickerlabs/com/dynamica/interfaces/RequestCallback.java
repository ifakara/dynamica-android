package wickerlabs.com.dynamica.interfaces;

/**
 * Created by Wickerman on 5/15/2017.
 */

public interface RequestCallback {

    public void onRequestComplete(String response, Exception e);
}
