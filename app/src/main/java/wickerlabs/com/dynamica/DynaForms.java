package wickerlabs.com.dynamica;

import android.app.Application;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.util.HashMap;

import wickerlabs.com.dynamica.model.Page;


public class DynaForms extends Application {

    private static final String ROOT_PATH = Environment.getDataDirectory() + File.separator + "dynaforms";
    private static final String STUDY_PATH = ROOT_PATH + File.separator + "study";
    private static final String DOWNLOADED_FORMS = ROOT_PATH + File.separator + "forms";
    private static final HashMap<String,Page> pageIdMap = new HashMap<>();
    private static final HashMap<Integer,Integer> formPageMap = new HashMap<>();
    private static final HashMap<String,String> imageFileMap = new HashMap<>();
    private static final HashMap<String, String> pageElementMap = new HashMap<>();

    public static String getPageIdByElement(String elementId) {
        return pageElementMap.get(elementId);
    }

    public static void addElementToPage(String elementId, String pageId) {
        pageElementMap.put(elementId, pageId);
    }

    public static void addFileMapping(String viewId, String filepath) {
        imageFileMap.put(viewId, filepath);
    }

    public static String getImageFile(String viewId){
        return imageFileMap.get(viewId);
    }

    public static boolean fileMapExists(String id) {
        return imageFileMap.containsKey(id);
    }

    public static void addPageMapping(int pageNum,Page page){
        String pgNumber = String.valueOf(pageNum);

        pageIdMap.put(pgNumber,page);
    }

    public static void addPageCount(int formId, int pageCount){
        formPageMap.put(formId,pageCount);
    }

    public static int getPageCount(int formId){
        return formPageMap.get(formId);
    }

    public static Page getPageByNumber(@NonNull int pageNum){
        String pgNumber = String.valueOf(pageNum);
        return pageIdMap.get(pgNumber);
    }

    public static void saveForm(){

    }

    public static void Log(String message){
        Log.d(DynaForms.class.getSimpleName(),message);
    }

    public static String getRootPath() {
        return ROOT_PATH;
    }

    public static String getDownloadsPath(String study) {
        return DOWNLOADED_FORMS;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //createDirs();
    }

    private void createDirs() {
        String dirs[] = {ROOT_PATH, STUDY_PATH};

        for (String dir : dirs) {
            File dirFile = new File(dir);

            if (!dirFile.exists()) {
                // create directories
                dirFile.mkdirs();
            }
        }
    }
}
