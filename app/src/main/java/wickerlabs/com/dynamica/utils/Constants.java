package wickerlabs.com.dynamica.utils;

/**
 * Created by: Yusuph Wickama
 * Date: 5/15/2017.
 * Time: 4:26 AM,
 * Website: https://www.wickerlabs.com
 */

public class Constants {

    public static String RABIES_STUDY = "study_rabies";
    public static String DEFAULT_CLIENT = "mobile";
    private static String ROOT_URL = "https://api.wickerlabs.com/";
    public static String BASE_URL = ROOT_URL + "dynamica/";
    public static String RETRIEVE_URL = BASE_URL+"retrieve.php";


}
