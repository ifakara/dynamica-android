package wickerlabs.com.dynamica.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.ContextThemeWrapper;

import wickerlabs.com.dynamica.R;


/**
 * Created by: WickerLabs.Inc
 * Time: 12:19 AM
 * Date: 2/13/2017
 * Website: http://www.wickerlabs.com
 */

public class DialogUtils {

    private static ProgressDialog dialog;
    private static DialogUtils instance;

    private DialogUtils() {

    }

    public static DialogUtils getInstance() {

        if(instance == null){
            instance = new DialogUtils();
            return instance;
        } else {
            return instance;
        }
    }

    public void startProgress(Context context, String message) {
        dialog = new ProgressDialog(new ContextThemeWrapper(context, R.style.myDialog));
        dialog.setMessage(message);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        dialog.show();
    }

    public boolean isShowing(){
        return dialog.isShowing();
    }

    public void stopProgress() {
        dialog.dismiss();
    }
}
