package wickerlabs.com.dynamica.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import wickerlabs.com.dynamica.layout.DynamicLinearLayout;
import wickerlabs.com.dynamica.model.Element;
import wickerlabs.com.dynamica.model.OfferedAnswers;
import wickerlabs.com.dynamica.model.Page;
import wickerlabs.com.dynamica.model.PageFlow;
import wickerlabs.com.dynamica.model.Question;

/**
 * Created by: Yusuph Wickama
 * Date: 6/5/2017.
 * Time: 9:35 PM,
 * Website: https://www.wickerlabs.com
 */

public class GlobalParser {
    private static final GlobalParser ourInstance = new GlobalParser();

    private GlobalParser() {
    }

    private static GlobalParser getInstance() {
        return ourInstance;
    }

    public static ArrayList<Page> parsePage(JSONObject formSchema) throws JSONException{
        JSONArray pages = formSchema.getJSONArray("pages");
        ArrayList<Page> pageArrayList= new ArrayList<>();

        for(int i = 0; i<pages.length();i++){
            JSONObject object = pages.getJSONObject(i);
            JSONObject pageFlow = new JSONObject(object.getString("pageFlow"));

            boolean formSubmit = !pageFlow.isNull("formSubmit") && pageFlow.getBoolean("formSubmit");
            boolean hasFirst = !object.isNull("isFirst") && object.getBoolean("isFirst");
            boolean hasLast = !object.isNull("isLast") && object.getBoolean("isLast");

            PageFlow flow = new PageFlow(!formSubmit, formSubmit);

            Page page = new Page();
            page.setId(object.getString("id"));
            page.setName(object.getString("name"));
            page.setDescription(object.getString("description"));
            page.setPageNumber(object.getInt("number"));
            page.setNamedPage(object.getBoolean("namedPage"));
            page.setPageFlow(flow);
            page.setElements(parseElements(object.getString("elements")));

            if(hasFirst) page.setFirst(object.getBoolean("isFirst"));

            if(hasLast) page.setLast(object.getBoolean("isLast"));

            pageArrayList.add(page);
        }

        return pageArrayList;
    }

    private static ArrayList<Element> parseElements(String schema) throws JSONException{
        ArrayList<Element> elements = new ArrayList<>();
        JSONArray jsonElements = new JSONArray(schema);

        for(int j = 0; j<jsonElements.length();j++){
            JSONObject object = jsonElements.getJSONObject(j);

            Element element = new Element();
            element.setId(object.getString("id"));
            element.setOrderNum(object.getInt("orderNo"));
            element.setType(object.getString("type"));
            element.setQuestion(parseQuestion(object.getString("question")));

            elements.add(element);
        }

        return elements;
    }

    private static Question parseQuestion(String schema) throws JSONException{
        JSONObject object = new JSONObject(schema);

        boolean modifiesFlow = !object.isNull("pageFlowModifier") && object.getBoolean("pageFlowModifier");

        Question question = new Question();
        question.setId(object.getString("id"));
        question.setRequired(object.getBoolean("required"));
        question.setModifiesFlow(modifiesFlow);
        question.setType(object.getString("type"));
        question.setQuestion(object.getString("text"));

        if(!object.isNull("offeredAnswers")){
            question.setOfferedAnswers(parseAnswers(object.getString("offeredAnswers"),modifiesFlow));
        }

        return question;
    }

    private static ArrayList<OfferedAnswers> parseAnswers(String schema, boolean modifiesFlow) throws JSONException{
        JSONArray answers = new JSONArray(schema);

        ArrayList<OfferedAnswers> offeredAnswers = new ArrayList<>();

        for(int i=0; i <answers.length();i++){
            JSONObject object = answers.getJSONObject(i);

            OfferedAnswers mAnswers = new OfferedAnswers();
            mAnswers.setId(object.getString("id"));
            mAnswers.setOrderNum(object.getInt("orderNo"));
            mAnswers.setValue(object.getString("value"));

            Log.d("[+] Object",object.toString());

            if(modifiesFlow) {
                mAnswers.setModifiesFlow(true);
                JSONObject flow = new JSONObject(object.getString("pageFlow"));
                Log.d("[+] Flow",flow.toString());

                if(flow.isNull("formSubmit") && flow.isNull("nextPage")){
                    // Means that the answer element has a page skip value
                    JSONObject skipToPage = new JSONObject(flow.getString("page"));
                    Log.d("[+] SkipPage",skipToPage.toString());
                    OfferedAnswers.SkipPage skipPage = new OfferedAnswers.SkipPage(skipToPage.getString("id"), skipToPage.getInt("number"));
                    mAnswers.setSkipPage(skipPage);
                } else {
                    mAnswers.setSkipPage(new OfferedAnswers.SkipPage(null, DynamicLinearLayout.NO_NEXT_PAGE));
                }
            }

            offeredAnswers.add(mAnswers);
        }

        return offeredAnswers;

    }


}
