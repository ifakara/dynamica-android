package wickerlabs.com.dynamica.handlers;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import wickerlabs.com.dynamica.R;
import wickerlabs.com.dynamica.widgets.DynamicCheckBox;
import wickerlabs.com.dynamica.widgets.DynamicEditText;
import wickerlabs.com.dynamica.widgets.DynamicImageView;
import wickerlabs.com.dynamica.widgets.DynamicSpinner;


public class WidgetsProvider {
    private static final WidgetsProvider ourInstance = new WidgetsProvider();

    private WidgetsProvider() {

    }

    public static WidgetsProvider getInstance() {
        return ourInstance;
    }

    public DynamicSpinner getSpinner(Activity activity) {
        View view = View.inflate(activity.getBaseContext(), R.layout.form_objects, null);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.formParent);

        DynamicSpinner spinner = (DynamicSpinner) view.findViewById(R.id.spinner);

        layout.removeView(spinner);

        return spinner;
    }

    public TextView getLabel(Activity activity, String text) {
        View view = View.inflate(activity.getBaseContext(), R.layout.form_objects, null);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.formParent);

        TextView label = (TextView) view.findViewById(R.id.textView);
        label.setTypeface(Typeface.DEFAULT_BOLD);
        label.setText(text);

        layout.removeView(label);

        return label;
    }

    public DynamicEditText getTextField(Activity activity) {
        View view = View.inflate(activity.getBaseContext(), R.layout.form_objects, null);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.formParent);

        DynamicEditText editText = (DynamicEditText) view.findViewById(R.id.editText);
        editText.setId(View.generateViewId());
        editText.setMaxLines(1);

        layout.removeView(editText);

        return editText;
    }

    public DynamicCheckBox getCheckBox(Activity activity, ArrayList<String> items) {
        View view = View.inflate(activity.getBaseContext(), R.layout.form_objects, null);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.formParent);

        DynamicCheckBox checkBox = (DynamicCheckBox) view.findViewById(R.id.checkBox);
        checkBox.setId(View.generateViewId());

        for(String item : items) checkBox.setText(item);

        layout.removeView(checkBox);

        return checkBox;
    }

    public DynamicImageView getImageView(Activity activity){
        View view = View.inflate(activity.getBaseContext(), R.layout.form_objects, null);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.formParent);

        DynamicImageView imageView = (DynamicImageView) view.findViewById(R.id.imageView);
        imageView.setId(View.generateViewId());

        layout.removeView(imageView);

        return imageView;

    }

}
