package wickerlabs.com.dynamica.handlers;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import wickerlabs.com.dynamica.DynaForms;
import wickerlabs.com.dynamica.structures.FormSubmit;
import wickerlabs.com.dynamica.widgets.DynamicCheckBox;
import wickerlabs.com.dynamica.widgets.DynamicEditText;
import wickerlabs.com.dynamica.widgets.DynamicImageView;
import wickerlabs.com.dynamica.widgets.DynamicSpinner;

/**
 * Created by: Yusuph Wickama
 * Date: 6/8/2017.
 * Time: 2:14 PM,
 * Website: https://www.wickerlabs.com
 */

public class FormSubmitter implements FormSubmit {

    private static final FormSubmitter ourInstance = new FormSubmitter();
    private static HashMap<String, JSONArray> dataArray = new HashMap<>();
    private static HashMap<String, JSONObject> dataCollection = new HashMap<>();
    private static HashMap<String, String> fieldResponseMap = new HashMap<>();
    private static HashMap<String, String> fieldPersistenceMap = new HashMap<>();

    private FormSubmitter() {
    }

    public static FormSubmitter getInstance() {
        return ourInstance;
    }

    public void updateValue(String fieldId, String value) {
        fieldPersistenceMap.put(fieldId, value);
    }

    public boolean persists(String id) {
        return fieldPersistenceMap.containsKey(id);
    }

    private boolean exists(String fieldId) {
        return fieldResponseMap.containsKey(fieldId);
    }

    public String getValue(String fieldId) {
        return fieldPersistenceMap.get(fieldId);
    }

    @Override
    public boolean commitForm(ViewGroup parent, String formName, int formRev, int formId, boolean isFinal) throws JSONException {

        JSONObject submittedForm = new JSONObject();

        JSONArray array = new JSONArray();
        String objectKey = formName.concat(String.valueOf(formId));
        String arrayKey = "Arr".concat(objectKey);

        if (dataCollection.containsKey(objectKey)) {
            submittedForm = dataCollection.get(objectKey);
        } else {
            dataCollection.put(objectKey, submittedForm);
        }

        if (dataArray.containsKey(arrayKey)) {
            array = dataArray.get(arrayKey);
        } else {
            dataArray.put(arrayKey, array);
        }

        int viewCount = parent.getChildCount();

        for (int i = 0; i < viewCount; i++) {
            View view = parent.getChildAt(i);
            JSONObject object = new JSONObject();

            if (view instanceof DynamicEditText) {
                // handle textField
                DynamicEditText editText = (DynamicEditText) view;

                if (editText.isInvalid()) {
                    return false;
                }

                // Put object in array only if its not present
                if (!exists(editText.getID())) {
                    object.put("fieldId", editText.getID());
                    object.put("question", editText.getQuestion());
                    array.put(object);
                }

                fieldResponseMap.put(editText.getID(), editText.getText().toString());

            } else if (view instanceof DynamicCheckBox) {
                // handle checkbox
                DynamicCheckBox checkBox = (DynamicCheckBox) view;

                if (!exists(checkBox.getID())) {
                    object.put("fieldId", checkBox.getID());
                    object.put("question", checkBox.getQuestion());
                    array.put(object);
                }

                fieldResponseMap.put(checkBox.getID(), checkBox.getText().toString());

            } else if (view instanceof DynamicSpinner) {
                // handle spinner
                DynamicSpinner spinner = (DynamicSpinner) view;

                if (!exists(spinner.getID())) {
                    object.put("fieldId", spinner.getID());
                    object.put("question", spinner.getQuestion());
                    array.put(object);
                }

                fieldResponseMap.put(spinner.getID(), spinner.getSelectedItem().toString());

            } else if (view instanceof DynamicImageView) {
                DynamicImageView imageView = (DynamicImageView) view;

                if (imageView.isInvalid()) {
                    Toast.makeText(parent.getContext(), "You need to provide a photo", Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (!exists(imageView.getID())) {
                    object.put("fieldId", imageView.getID());
                    object.put("question", imageView.getQuestion());
                    array.put(object);
                }

                fieldResponseMap.put(imageView.getID(), imageView.getImagePath());
            }
        }

        if (isFinal) {
            int length = array.length();
            JSONArray finalArray = new JSONArray();

            for (int j = 0; j < length; j++) {
                JSONObject jsonObject = array.getJSONObject(j);
                String fieldId = jsonObject.getString("fieldId");
                jsonObject.put("response", fieldResponseMap.get(fieldId));

                DynaForms.Log("[+] Old object: " + jsonObject);

                finalArray.put(jsonObject);
            }

            submittedForm.put("formId", formId);
            submittedForm.put("formName", formName);
            submittedForm.put("formRevision", formRev);
            submittedForm.put("submittedData", finalArray);

            Log.d("[+] Form submission", submittedForm.toString());
        }

        return true;
    }

    @Override
    public void removePageResponse(ViewGroup layout, String pageId, String formName, int formId) throws JSONException {
        JSONArray array = new JSONArray();
        String objectKey = formName.concat(String.valueOf(formId));
        String arrayKey = "Arr".concat(objectKey);

        if (dataArray.containsKey(arrayKey)) {
            array = dataArray.get(arrayKey);
        } else {
            dataArray.put(arrayKey, array);
        }

        for (int j = 0; j < array.length(); j++) {
            JSONObject jsonObject = array.getJSONObject(j);
            String fieldId = jsonObject.getString("fieldId");

            String fieldPageId = DynaForms.getPageIdByElement(fieldId);

            if (fieldPageId.equals(pageId)) {
                fieldResponseMap.remove(fieldId);

                array.remove(j);
                DynaForms.Log("[+] Removed responses from ".concat(pageId));
            }

        }


    }
}
