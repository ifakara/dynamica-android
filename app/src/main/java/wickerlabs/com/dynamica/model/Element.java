package wickerlabs.com.dynamica.model;

/**
 * Created by: Yusuph Wickama
 * Date: 6/5/2017.
 * Time: 9:35 PM,
 * Website: https://www.wickerlabs.com
 */

public class Element {

    private String id,type;
    private int orderNum;
    private Question question;


    public Element() {
    }


    public Element(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public Element(Question question) {
        this.question = question;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }




}
