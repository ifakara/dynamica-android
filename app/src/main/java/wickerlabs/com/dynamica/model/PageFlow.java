package wickerlabs.com.dynamica.model;

/**
 * Created by: Yusuph Wickama
 * Date: 6/5/2017.
 * Time: 9:35 PM,
 * Website: https://www.wickerlabs.com
 */

public class PageFlow {
    private boolean nextPage,formSubmit;
    private Page nextSkipPage;


    public PageFlow(boolean nextPage, boolean formSubmit) {
        this.nextPage = nextPage;
        this.formSubmit = formSubmit;
    }

    public void setAfterPage(String pageId, int pageNum) {
        this.nextSkipPage = nextSkipPage;
    }

    public Page getNextSkipPage() {
        return nextSkipPage;
    }

    public boolean isNextPage() {
        return nextPage;
    }

    public boolean isFormSubmit() {
        return formSubmit;
    }
}
