package wickerlabs.com.dynamica.model;

/**
 * Created by Wickerman on 6/5/2017.
 */

public class OfferedAnswers {

    private String id,value;
    private int orderNum;
    private SkipPage skipPage;
    private boolean modFlow = false;

    public OfferedAnswers() {

    }

    public void setModifiesFlow(boolean modifiesFlow){
        this.modFlow = modifiesFlow;
    }

    public boolean modifiesFlow(){
        return this.modFlow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public SkipPage getSkipPage() {
        return skipPage;
    }

    public void setSkipPage(SkipPage skipPage) {
        this.skipPage = skipPage;
    }

    public static class SkipPage{
        String id;
        int pageNum;

        public SkipPage(String id, int pageNum) {
            this.id = id;
            this.pageNum = pageNum;
        }

        public String getId() {
            return id;
        }

        public int getPageNum() {
            return pageNum;
        }
    }
}
