package wickerlabs.com.dynamica.model;

import java.util.ArrayList;

/**
 * Created by Wickerman on 6/5/2017.
 */

public class Page {

    private String id,name, description;
    private int pageNumber;
    private ArrayList<Element> elements;
    private PageFlow pageFlow;
    private boolean isNamedPage,isFirst,isLast;
    private int previousPageNumber;

    public Page() {

    }

    public int getPreviousPageNumber() {
        return previousPageNumber;
    }

    public void setPreviousPageNumber(int previousPageNumber) {
        this.previousPageNumber = previousPageNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public ArrayList<Element> getElements() {
        return elements;
    }

    public void setElements(ArrayList<Element> elements) {
        this.elements = elements;
    }

    public PageFlow getPageFlow() {
        return pageFlow;
    }

    public void setPageFlow(PageFlow pageFlow) {
        this.pageFlow = pageFlow;
    }

    public boolean isNamedPage() {
        return isNamedPage;
    }

    public void setNamedPage(boolean namedPage) {
        isNamedPage = namedPage;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }


}
