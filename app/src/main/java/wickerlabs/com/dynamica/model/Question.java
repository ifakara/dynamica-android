package wickerlabs.com.dynamica.model;

import java.util.ArrayList;

/**
 * Created by Wickerman on 6/5/2017.
 */

public class Question {

    private String id,type,question;
    private boolean hasPageFlow,isRequired;
    private ArrayList<OfferedAnswers> offeredAnswers;
    private boolean modifiesFlow;

    public Question() {
    }

    public Question(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public boolean isModifiesFlow() {
        return modifiesFlow;
    }

    public void setModifiesFlow(boolean modifiesFlow) {
        this.modifiesFlow = modifiesFlow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean hasPageFlow() {
        return hasPageFlow;
    }

    public void setHasPageFlow(boolean hasPageFlow) {
        this.hasPageFlow = hasPageFlow;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public ArrayList<OfferedAnswers> getOfferedAnswers() {
        return offeredAnswers;
    }

    public void setOfferedAnswers(ArrayList<OfferedAnswers> offeredAnswers) {
        this.offeredAnswers = offeredAnswers;
    }

    @Override
    public String toString() {
        return this.getQuestion();
    }
}
