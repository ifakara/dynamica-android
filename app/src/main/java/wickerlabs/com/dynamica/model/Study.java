package wickerlabs.com.dynamica.model;

/**
 * Created by yusuphwickama on 7/26/17.
 */

public class Study {
    private String name;
    private int id;

    public Study(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
