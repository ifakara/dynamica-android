package wickerlabs.com.dynamica.model;

/**
 * Created by Wickerman on 5/14/2017.
 */

public class DynamicForm {

    private String name,description,modified;
    private int id,accessLevel;
    private String schema;
    private int revision;
    private Study formStudy;

    public DynamicForm() {
    }

    public DynamicForm(int id, String name, int accessLevel) {
        this.id = id;
        this.name = name;
        this.accessLevel = accessLevel;
    }

    public Study getFormStudy() {
        return formStudy;
    }

    public void setFormStudy(Study formStudy) {
        this.formStudy = formStudy;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }
}
